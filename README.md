# Collabsphere-2019-DEV112 by Howard Greenberg and Jesse Gallagher

This is the demo database from our session at Collabspher e-2019 called DEV112 - Let&#39;s Calendar That! - Add Calendars to your Domino web applications.

View the presentation at https://www.slideshare.net/HowardGreenberg/dev112-lets-calendar-that

This project requires the OpenNTF Domino API, XPages Jakarta EE support, and the OpenNTF debug toolbar. You can remove the references to the debugtoolbar in the Java code and XPages to remove this dependency.

If you don't want to install the OpenNTF ODA project you will have to modify the code to use the standard Java API provided in HCL Notes and Domino.

For more information on the Debug Toolbar go to:
https://www.openntf.org/main.nsf/project.xsp?r=project/XPage%20Debug%20Toolbar

For more information on the OpenNTF ODA project go to:
https://oda.openntf.org/main.nsf/project.xsp?r=project/OpenNTF%20Domino%20API

For more information on the XPages Jakarta EE project, go to:
https://openntf.org/main.nsf/project.xsp?r=project/XPages%20Jakarta%20EE%20Support
