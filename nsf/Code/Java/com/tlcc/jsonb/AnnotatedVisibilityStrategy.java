package com.tlcc.jsonb;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.config.PropertyVisibilityStrategy;

/**
 * JSON-B visibility strategy that only outputs fields that have explicit
 * JsonbProperty annotations.
 * 
 * @author Jesse Gallagher
 */
public class AnnotatedVisibilityStrategy implements PropertyVisibilityStrategy {

	@Override
	public boolean isVisible(Field field) {
		return field.getAnnotation(JsonbProperty.class) != null;
	}

	@Override
	public boolean isVisible(Method method) {
		return method.getAnnotation(JsonbProperty.class) != null;
	}

}
