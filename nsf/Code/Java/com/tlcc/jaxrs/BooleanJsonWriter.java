package com.tlcc.jaxrs;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.json.Json;
import javax.json.JsonWriter;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;

public class BooleanJsonWriter implements MessageBodyWriter<Boolean> {

	@Override
	public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		return MediaType.APPLICATION_JSON_TYPE.equals(mediaType) && (boolean.class.equals(genericType) || Boolean.class.equals(genericType));
	}

	@Override
	public void writeTo(Boolean t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType,
			MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream)
			throws IOException, WebApplicationException {
		
		JsonWriter w = Json.createWriter(entityStream);
		w.write(
			Json.createObjectBuilder()
				.add("success", t.booleanValue())
				.build()
		);
	}

}
