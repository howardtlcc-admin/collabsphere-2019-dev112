package com.tlcc.jaxrs;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import lotus.domino.NotesException;

@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {

	@Override
	public Response toResponse(Throwable t) {
		if (t instanceof WebApplicationException) {
			WebApplicationException e = (WebApplicationException) t;
			if (e.getResponse() != null) {
				return e.getResponse();
			} else {
				return createResponseFromException(t, 422);
			}
		} else {
			return createResponseFromException(t, 422);
		}
	}

	private Response createResponseFromException(Throwable throwable, int status) {
		return Response.status(status)
			.type(MediaType.APPLICATION_JSON_TYPE)
			.entity((StreamingOutput)(out) -> {
				String message = ""; //$NON-NLS-1$
				Throwable t = throwable;
				while ((message == null || message.length() == 0) && t != null) {
					if (t instanceof NotesException) {
						message = ((NotesException) t).text;
					} else {
						message = t.getMessage();
					}

					t = t.getCause();
				}
				
				JsonArrayBuilder st = Json.createArrayBuilder();
				for(StackTraceElement el : throwable.getStackTrace()) {
					st.add(el.toString());
				}
				
				JsonWriter w = Json.createWriter(out);
				w.writeObject(Json.createObjectBuilder()
					.add("success", false)
					.add("errorMessage",message)
					.add("stackTrace", st.build())
					.build());
			})
			.build();
	}

}
