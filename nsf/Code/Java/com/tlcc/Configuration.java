package com.tlcc;

import java.io.Serializable;

import javax.faces.context.FacesContext;
public class Configuration implements Serializable {
	
	
	/**
	 * This is used to set application scope config type variables
	 */
	private static final long serialVersionUID = 1L;
	String jsonFeedURL;
	 
	final String  XPagesJSON = "/API.xsp/API";
	final String JSONB = "/xsp/.jaxrs/calendar";
	private String path ;
	private String XPagesJSONurl ;
	private String JSONBurl;
	private boolean usingXPages;
	public Configuration() {
		
		
		this.path = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
		//set the different JSON urls
		this.XPagesJSONurl =  this.path +  XPagesJSON;
		this.JSONBurl = this.path + JSONB;
		//start off with XPages based JSON feed
		this.usingXPages = true;
		this.jsonFeedURL = this.XPagesJSONurl;
	}


	public String getJsonFeedURL() {
		return jsonFeedURL;
	}


	public void setJsonFeedURL(String jsonFeedURL) {
		this.jsonFeedURL = jsonFeedURL;
	}
	
	public void toggleJSONURL() {
		//toggle between the two
		if (usingXPages){
			//switch to JSONB
			this.jsonFeedURL = this.JSONBurl;
			this.usingXPages = false;
		} else {
			this.jsonFeedURL = this.XPagesJSONurl;
			this.usingXPages = true;
		}
	}


	public boolean isUsingXPages() {
		return usingXPages;
	}


	public void setUsingXPages(boolean usingXPages) {
		this.usingXPages = usingXPages;
	}
}
