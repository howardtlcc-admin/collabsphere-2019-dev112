package com.tlcc;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

import javax.json.bind.annotation.JsonbDateFormat;
import javax.json.bind.annotation.JsonbProperty;

import org.openntf.xsp.debugtoolbar.beans.DebugToolbarBean;

import org.openntf.domino.*;
public class Calendar implements Serializable{
	private static final long serialVersionUID = 1L;

	private static final int defaultMorningTime = 8;  //sets a default start time for all day events
	private static final int defaultEveningTime = 17;//sets a default end time for all day events
	@JsonbProperty("title")
	String name;
	Date startDate;
	Date startTime;
	@JsonbProperty("start") @JsonbDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
	Date startDateTime;
	Date endDate;
	Date endTime;
	@JsonbProperty("end") @JsonbDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
	Date endDateTime;
	String importantStr; //store a "1" if important, "0" if not, use isImportant to get boolean
	@JsonbProperty
	String description;
	@JsonbProperty
	String color;  //color of cal entry
	
	String allDayStr; //store a "1" if all day event, "0" if not, use isAllDay to get boolean
	@JsonbProperty("id")
	String unid; //unid of document
	boolean newDocument; //true if a new document
	
	ArrayList<String> messages;
	
	public Calendar() {
		//setup variables
		resetValues();
	}
	
	private void resetValues() {
		// reset all values back to empty or null
		this.name = "";
		this.importantStr = "0";
		this.description = "";
		this.allDayStr = "0";
		this.newDocument = true;
		this.startDate = null;
		this.startTime = null;
		this.startDateTime = null;
		this.endDate = null;
		this.endTime = null;
		this.endDateTime = null;
		this.unid = "";
		this.messages = new ArrayList<String>();
		this.color = "blue";
	}

	public Calendar(String name, String importantStr, String description, boolean newDocument, String allDayStr, Date startDate,
			Date startTime, Date startDateTime, Date endDate, Date endTime, Date endDateTime, String unid){
		//constructor to pop values
		this.name = name;
		this.importantStr = importantStr;
		this.description = description;
		this.allDayStr = allDayStr;
		this.newDocument = newDocument;
		this.startDate = startDate;
		this.startTime = startTime;
		this.startDateTime = startDateTime;
		this.endDate = endDate;
		this.endTime = endTime;
		this.endDateTime = endDateTime;
		this.unid = unid;
		this.messages = new ArrayList<String>();
		this.color = "blue";
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public Date getStartDateTime() {
		return startDateTime;
	}
	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Date getEndDateTime() {
		return endDateTime;
	}
	public void setEndDateTime(Date endDateTime) {
		this.endDateTime = endDateTime;
	}
	public String getImportantStr() {
		return importantStr;
	}
	public void setImportantStr(String importantStr) {
		this.importantStr = importantStr;
	}
	public boolean isImportant() {
		boolean rtnIsImportant = false;
		if (importantStr.equalsIgnoreCase("1")){
			rtnIsImportant = true;
		}
		return rtnIsImportant;
	}
	public void setImportant(boolean important) {
		if (important){
			this.importantStr = "1"; 
		} else {
			this.importantStr = "0";
		}
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAllDayStr() {
		return allDayStr;
	}
	public void setAllDayStr(String allDayStr) {
		this.allDayStr = allDayStr;
	}
	@JsonbProperty("allDay")
	public boolean isAllDay() {
		//based on what is in allDayStr
		boolean rtnAllDay = false;
		if (this.allDayStr.equalsIgnoreCase("1")){
			rtnAllDay = true;
		}
		return rtnAllDay;
	}
	
	public boolean isShowTimes(){
		//show the times when it is not an allday event
		return !this.isAllDay();
	}
	
	public String getUnid() {
		return unid;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public ArrayList<String> getMessages() {
		return messages;
	}

	public void setMessages(ArrayList<String> messages) {
		this.messages = messages;
	}

	public void clearMessages(){
		this.messages.clear();
	}
	
	public boolean isNewDocument() {
		return newDocument;
	}

	public void setNewDocument(boolean newDocument) {
		this.newDocument = newDocument;
	}

	public void setAllDay(boolean allDay) {
		//set the allDayStr based on the input param
		if (allDay){
			this.allDayStr = "1";
		} else {
			this.allDayStr = "0";
		}
	}
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	//method to retrieve existing calendar entry by unid
	public boolean getCalendarByUnid(Database db, String unid){
		//try to get the document
		boolean rtnBoolean = false;
		if (null != unid && !unid.isEmpty()){
			if (unid.equalsIgnoreCase("new")){
				//create a new calendar entry
				resetValues();
				
			} else {
				//get from database
				Document calDoc = db.getDocumentByUNID(unid);
				if (null != calDoc){
					this.name = calDoc.getItemValueString("name");
					this.startDate = calDoc.getItemValue("startDate", Date.class);
					this.startTime = calDoc.getItemValue("startTime", Date.class);
					this.startDateTime = calDoc.getItemValue("startDateTime", Date.class);
					this.endDate = calDoc.getItemValue("endDate", Date.class);
					this.endTime = calDoc.getItemValue("endTime", Date.class);
					this.endDateTime = calDoc.getItemValue("endDateTime", Date.class);
					this.importantStr = calDoc.getItemValueString("important");
					this.allDayStr = calDoc.getItemValueString("Allday");
					CalendarJSON.debugMsg("allday is " + this.allDayStr);
					CalendarJSON.debugMsg("isAllDay " + this.isAllDay());
					this.description = calDoc.getItemValueString("description");
					this.unid = unid;
					this.newDocument = false;
					rtnBoolean = true;
				} else {
					//document not found
				}
			}
		}
		return rtnBoolean;
	}
	
	
	

	//method to save object to Domino
	public boolean saveCalendar(Database db){
		boolean rtnStatus = false;
		
		try {
		if (null != db){
			Document calDoc ;
			//save to Domino
			if (this.isNewDocument()){
				//create a new document
				calDoc = db.createDocument();
			} else {
				//get the document by unid
				calDoc = db.getDocumentByUNID(this.unid);
			}
			if (null != calDoc){
				
				
				//set the combined date-time value for the start and end dates
				//if all day event ignore the time and just use a default time for the start
				Date actualStartTime = null;
				Date actualEndTime = null;
				//create a combined date-time for start and end
				
				
				if (this.isAllDay()){
					CalendarJSON.debugMsg("all day? " + this.isAllDay());
					//if the user picked full days than set the default time (08:00 and 17:00)
					actualStartTime = setDefaultTime(this.startDate, defaultMorningTime);
					actualEndTime = setDefaultTime(this.endDate, defaultEveningTime);	
					
				} else {
					//use times in the startTime and endTime as the actual times
					//combine into one value
					if (null!= this.startTime && null != this.endTime){
						//use the actual times
						actualStartTime = setTime(this.startDate, this.startTime);
						actualEndTime = setTime(this.endDate, this.endTime);
						
					} else {
						CalendarJSON.debugMsg("missing start or end time, using default values");
						actualStartTime = setDefaultTime(this.startDate, defaultMorningTime);
						actualEndTime = setDefaultTime(this.endDate, defaultEveningTime);
					}
				}
				//set the combined datetimes back to the document
				calDoc.replaceItemValue("startDateTime",actualStartTime);
				calDoc.replaceItemValue("endDateTime",actualEndTime);
				
				if (this.isAllDay()){
					//if all day then set the times to the default so they match the combined datetimes
					//set the default times in the startTime and endTime so they match the combined datetimes
					calDoc.replaceItemValue("startTime",actualStartTime);
					calDoc.replaceItemValue("endTime",actualEndTime);
				}
					
				CalendarJSON.debugMsg("new start date is " + actualStartTime.toString());
				CalendarJSON.debugMsg("new end date is " + actualEndTime.toString());
			
				//set the values for the document
				calDoc.replaceItemValue("name" , this.name);
				calDoc.replaceItemValue("description", this.description);
				calDoc.replaceItemValue("important", this.importantStr);
				calDoc.replaceItemValue("allDay", this.allDayStr);
				calDoc.replaceItemValue("startDate",this.startDate);
				calDoc.replaceItemValue("startTime",this.startTime);
				
				calDoc.replaceItemValue("endDate",this.endDate);
				calDoc.replaceItemValue("endTime",this.endTime);
				//save the document
				rtnStatus = calDoc.save();
				//update the FT index
				if (db.isFTIndexed()){
					//update the index
					db.updateFTIndex(false);
				}
			}
			
			//if it saves then set newEntry to false
			this.newDocument = false;
		
		}
		} catch (Exception e){
			DebugToolbarBean.get().error(e);
		}
		return rtnStatus;
	}
	
	//do search to find calendar docs meeting criteria
	public static ArrayList<Calendar> getCalendarDocs(Database calDb, String startParam, String endParam, String importantParam){
		ArrayList<Calendar> rtnList = new ArrayList<Calendar>();
		if (calDb.isFTIndexed()){
			//update the index
			calDb.updateFTIndex(false);
		}
		//build FT Search
		String ftSearch = buildFTSearch(startParam, endParam, importantParam);
		CalendarJSON.debugMsg("ftSearch " + ftSearch);
		if (calDb.isOpen()){
			if (!ftSearch.isEmpty()){
				//get the view
				View calVw = calDb.getView("CalendarEntries");
				if (null != calVw){
					//CalendarJSON.debugMsg("docs in view before " + calVw.getEntryCount());
					//apply ft search
					calVw.FTSearch(ftSearch);
					ViewEntryCollection vwEntryCol = calVw.getAllEntries();
					//CalendarJSON.debugMsg("docs in view after " + vwEntryCol.getCount());

					if (vwEntryCol.getCount()> 0){
						//build list of Calendar events	
						String name;
						String importantStr;
						String description;
						boolean newDocument = false; //will always be false loading from a view
						String allDayStr;
						Date startDate = new Date();
						Date  startTime = new Date();
						Date startDateTime;
						Date endDate = new Date();
						Date endTime = new Date();
						Date endDateTime;
						String unid = "";
						for (ViewEntry entry : vwEntryCol){
							//get the documents
							name = entry.getColumnValue("name",String.class);
							startDateTime = entry.getColumnValue("startdatetime" , Date.class);
							endDateTime = entry.getColumnValue("enddatetime",Date.class);
							description = entry.getColumnValue("Description",String.class);
							
							importantStr = entry.getColumnValue("Important",String.class);
							CalendarJSON.debugMsg("important String is " + importantStr);
							allDayStr = entry.getColumnValue("Allday", String.class);
							unid = entry.getUniversalID();
							Calendar tempCal = new Calendar(name, importantStr, description, newDocument, 
										allDayStr, startDate, startTime, startDateTime, endDate, endTime, endDateTime, unid);
							//fix ending date if allday
							//ending date is exclusive (see fullcalendar docs)
							if (tempCal.isAllDay()){
								tempCal.setEndDateTime(adjustEndingDateTime(tempCal.getEndDateTime()));
								CalendarJSON.debugMsg("adjusted end date to " + tempCal.getEndDateTime());
							}
							CalendarJSON.debugMsg("important " + tempCal.importantStr);
							//set the color of the entry
							if (tempCal.isImportant()){
								CalendarJSON.debugMsg("is important");
								tempCal.color = "red";
							} else if (tempCal.isAllDay()){
								tempCal.color = "green";
							}
							
							//add to list
							rtnList.add(tempCal);
						}
					}
				} else {
					//view not found
					DebugToolbarBean.get().info("view not found in getCalendarDocs in Calendar.java");
				}
			}
		} else {
			//db not found
			DebugToolbarBean.get().info("db is null getCalendarDocs in Calendar.java");
		}
		return rtnList;
	}
	
	 static String buildFTSearch(String startStr, String endStr, String importantStr){
		//dates will be in yyyy-mm-dd format
		//important will be 'yes' for important param, if so, only return important records
		String rtnFTSearch = "";
		//build the FT Search query
		try{
			//start date is the date fullcalendar requests, they need all entries
			//between the start and end date
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
			SimpleDateFormat sdfQuery = new SimpleDateFormat("mm-dd-yyyy");
			Date startDate = sdf.parse(startStr);
			Date endDate = sdf.parse(endStr);
			String reqStartDate = sdfQuery.format(startDate);
			String reqEndDate = sdfQuery.format(endDate);
			//first condition, cal entry is between start and end date
			StringBuilder query = new StringBuilder("(( Field startdatetime >= ");
			query.append(reqStartDate);
			query.append(" and Field enddatetime <= ");
			query.append(reqEndDate);
			query.append(")");
			//second condition, cal entry spans the requested start and end dates 
			query.append(" OR ");
			query.append("(Field startdatetime <= ");
			query.append(reqStartDate);
			query.append(" and Field enddatetime >= ");
			query.append(reqEndDate);
			query.append(")");
			//third condition, start date is in range
			query.append(" OR ");
			query.append("(Field startdatetime >= ");
			query.append(reqStartDate);
			query.append(" AND Field startdatetime <= ");
			query.append(reqEndDate);
			query.append(")");
			//fourth condition, end date is in range
			query.append(" OR ");
			query.append("(Field enddatetime >= ");
			query.append(reqStartDate);
			query.append(" AND Field enddatetime <= ");
			query.append(reqEndDate);
			query.append(")");
			//end paren
			query.append(")");
			//important param
			if (null != importantStr && !importantStr.isEmpty()){
				if (importantStr.equalsIgnoreCase("yes")){
					CalendarJSON.debugMsg("wants only important");
					query.append(" AND (FIELD Important = \"" + "1" + "\")");
				}
			}
			rtnFTSearch = query.toString();
		} catch (Exception e){
			//assume a date parsing issue
			//return an empty string
			rtnFTSearch = "";
			DebugToolbarBean.get().info("exception in buildFTSearch in Calendar.java");
			DebugToolbarBean.get().error(e);
		}
		return rtnFTSearch;
	}
	 
	 public boolean changeCalendarDates(Database calDb, String id, String newStartDate, String newEndDate){
		 boolean rtnBoolean = false;
		 //convert dates to Date
		 Date startDate = Date.from( Instant.parse( newStartDate ));
		 Date endDate = Date.from(Instant.parse(newEndDate));
		 CalendarJSON.debugMsg("converted dates are " + startDate.toString() + "  " + endDate.toString());
		 boolean valid = getCalendarByUnid(calDb, id);
		 if (valid){
			 //change date
			 CalendarJSON.debugMsg("test " + this.getName());
			 this.setStartDate(startDate);
			 this.setStartTime(startDate);
			 //add day events need to back down a day from what fullcalendar says
			 if (this.isAllDay()){
				 //adjust endDate back a day
				 endDate = reduceEndingDate(endDate);
			 }
			 this.setEndDate(endDate);
			 this.setEndTime(endDate);
			 //save the changes
			 if (this.saveCalendar(calDb)){
				 //saved ok
				 rtnBoolean = true;
			 }
			 
		 } 
		 CalendarJSON.debugMsg("save worked " + rtnBoolean);
		return rtnBoolean;
	 }
	 
	 
	 private Date setTime(Date date, Date time){
		 	//creates a new Date that has the date and time from the input params
			Date newDate = new Date();
			java.util.Calendar dateCal = java.util.Calendar.getInstance();
			java.util.Calendar timeCal = java.util.Calendar.getInstance();
			java.util.Calendar newDateCal = java.util.Calendar.getInstance();
			newDateCal.setTime(newDate);
			//first set the date to the desired date
			dateCal.setTime(date);
			timeCal.setTime(time);
			int year = dateCal.get(java.util.Calendar.YEAR);
			int month = dateCal.get(java.util.Calendar.MONTH);
			int day = dateCal.get(java.util.Calendar.DAY_OF_MONTH);
			int hour = timeCal.get(java.util.Calendar.HOUR_OF_DAY);
			int min = timeCal.get(java.util.Calendar.MINUTE);
			int sec = 0;
			
			//set the new date calendar
			newDateCal.set(year, month, day, hour, min, sec);
			//clean up the ms
			newDateCal.set(java.util.Calendar.MILLISECOND,0);
			CalendarJSON.debugMsg("new date is " + newDateCal.getTime().toString());
			
			return newDateCal.getTime();
		}
	 
	 private Date setDefaultTime(Date date, int hours){
		 	//creates a new Date from the input param and adds a default time for the morning
		 	java.util.Calendar dateCal = java.util.Calendar.getInstance();
			//first set the date to the desired date
			dateCal.setTime(date);
			//set the hours
			dateCal.set(java.util.Calendar.HOUR_OF_DAY,hours);
			dateCal.set(java.util.Calendar.MINUTE,0);
			dateCal.set(java.util.Calendar.MILLISECOND,0);
			//return the new date value
			return dateCal.getTime();
		}
		
	 private static Date adjustEndingDateTime(Date date){
		 	//creates a new Date from the input param and adds a day to it
		 	java.util.Calendar dateCal = java.util.Calendar.getInstance();
			//first set the date to the desired date
			dateCal.setTime(date);
			//add a day
			dateCal.add(java.util.Calendar.DATE,1);
			//set the hours
			dateCal.set(java.util.Calendar.HOUR_OF_DAY,0);
			dateCal.set(java.util.Calendar.MINUTE,0);
			dateCal.set(java.util.Calendar.MILLISECOND,0);
			//return the new date value
			return dateCal.getTime();
		}
	 
	 private static Date reduceEndingDate(Date date){
		 	//creates a new Date from the input param and adds a day to it
		 	java.util.Calendar dateCal = java.util.Calendar.getInstance();
			//first set the date to the desired date
			dateCal.setTime(date);
			//add a day
			dateCal.add(java.util.Calendar.DATE,-1);
			//return the new date value
			return dateCal.getTime();
		}
	 
	 
	 
	 	public boolean validateDateFields(){
	 		boolean rtnBoolean = true;
	 		if (!validateDate(this.startDate)){
	 			rtnBoolean = false;
	 			this.messages.add("Enter the start date.");
	 		}
	 		if (!validateDate(this.endDate)){
	 			rtnBoolean = false;
	 			this.messages.add("Enter the end date.");
	 		}
	 		//only care about times is not allday
	 		if (!this.isAllDay()){
	 			if (!validateDate(this.endTime)){
		 			rtnBoolean = false;
		 			this.messages.add("Enter the end time.");
		 		}
	 			if (!validateDate(this.startTime)){
		 			rtnBoolean = false;
		 			this.messages.add("Enter the start time.");
		 		}
	 		}
	 		
	 		return rtnBoolean;
	 	}
	 
	 	private boolean validateDate(Date val){
	 		boolean rtnBoolean = true;
	 		if (null == val){
	 			rtnBoolean = false;
	 		}
	 		return rtnBoolean;
	 	}
	 	
		public boolean validateDates(){
			Date startDate = this.startDate;
			Date endDate = this.endDate;
			Date startTime = this.startTime;
			Date endTime = this.endTime;
			boolean rtnBoolean = false;
			CalendarJSON.debugMsg("validatedates" + endDate.toString());
			try{
				
				CalendarJSON.debugMsg("start date is " + startDate.toString());
				CalendarJSON.debugMsg(" end date " + endDate.toString());
				if (endDate.after(startDate)){
					CalendarJSON.debugMsg("passed validation!!!");
					rtnBoolean = true;
				} else if (startDate.compareTo(endDate) == 0){
						//the dates are the same, compare the times unless it is all day
					if (this.isAllDay()){
						//the times don't matter
						CalendarJSON.debugMsg("the dates are the same, that is ok");
						rtnBoolean = true;
					} else {
						//make sure the times are different
						if (endTime.after(startTime)){
							//that is good
							rtnBoolean = true;
						} else {
							CalendarJSON.debugMsg("the times are different ");
						}
					}
				}
				
			if (!rtnBoolean){
				//else failed validation
				this.messages.add("The end date and time must be after the start date and time.");
				CalendarJSON.debugMsg("failed validation - validateDates");
			}
				
			} catch (Exception e) {
				DebugToolbarBean.get().info("exception in validateDates in Calendar.java");
				DebugToolbarBean.get().error(e);
			}
			return rtnBoolean;
		}
	 
}
