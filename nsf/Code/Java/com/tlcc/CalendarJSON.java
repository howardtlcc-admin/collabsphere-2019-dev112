package com.tlcc;

import java.io.BufferedReader;
import java.io.StringWriter;

import java.util.ArrayList;

import java.util.Map;

import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openntf.domino.Database;
import org.openntf.domino.Session;
import org.openntf.domino.utils.Factory;
import org.openntf.domino.utils.Factory.SessionType;
import org.openntf.xsp.debugtoolbar.beans.DebugToolbarBean;

import com.ibm.domino.services.ServiceException;
import com.ibm.domino.services.rest.RestServiceEngine;
import com.ibm.domino.services.util.JsonWriter;
import com.ibm.xsp.extlib.component.rest.CustomService;
import com.ibm.xsp.extlib.component.rest.CustomServiceBean;
import com.ibm.xsp.extlib.util.ExtLibUtil;

import com.ibm.commons.util.io.json.*;

public class CalendarJSON extends CustomServiceBean {

	private static boolean showDBar = true;
	
	private Session session;
	public final static String STATUS_SUCCESS = "success";
	public final static String STATUS_FAIL = "fail";
	public final static String STATUS_ERROR = "error";

	public final static String JSON_RESPONSE_STATUS = "status";
	public final static String JSON_RESPONSE_DATA = "data";
	public final static String JSON_RESPONSE_MESSAGE = "message";

	//private final static String REST_PATH = "/cal/";

	private JsonWriter jsonWriter;
	private StringWriter sw;
	

	@Override
	public void renderService(CustomService service, RestServiceEngine engine)
			throws ServiceException {

		try {
			HttpServletRequest request = engine.getHttpRequest();
			HttpServletResponse response = engine.getHttpResponse();
			response.setHeader("Content-Type",
					"application/json; charset=UTF-8");

			// use StringWriter instead of response.getWriter() so that in case
			// of an error the ServletOutputStream is not blocked from the
			// PrintWriter.
			this.sw = new StringWriter();
			this.jsonWriter = new JsonWriter(sw, false);
			session = Factory.getSession(SessionType.CURRENT);
			String method = request.getMethod();
			if (method.equals("GET")) {
				this.doGet(request, response);
			} else if (method.equals("POST")) {
				this.doPost(request, response);
			} else if (method.equals("PUT")) {
				this.doPut(request, response);
			} else if (method.equals("DELETE")) {
				this.doDelete(request, response);
			}

			sw.close();
			response.getWriter().write(sw.toString());
			response.getWriter().close();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}


	private void doGet(HttpServletRequest request, HttpServletResponse response) {
		
		Database calDb = null;
		
		try {
			
			//this.createSuccessJsonWriter();
			//parse the parameters
		//	String qs = request.getQueryString();
			//display parameters
		//	debugMsg("qs is " + qs);
			
			
			String importantStr =  request.getParameter("important");
			String startReqDateInpStr = request.getParameter("start");
			String endReqDateInpStr = request.getParameter("end");
			
		//	debugMsg("start is "  + startReqDateInpStr);
		//	debugMsg("end is "  + endReqDateInpStr);
						
			
			calDb = session.getCurrentDatabase();
			
			if (null == calDb){
				debugMsg("calDb is null");
			}
			ArrayList<Calendar> calList = Calendar.getCalendarDocs(calDb, startReqDateInpStr,endReqDateInpStr, importantStr);
					
			if (!calList.isEmpty()){	
				/*
				boolean fullDayRequest = false;
				String startDateStr;
				String endDateStr;
					
				String title = "test title";
				String unid = "DFSFSF#####%%%55565656";
				Date tempStartDate = new Date();
				debugMsg("current date is " + tempStartDate.toLocaleString());
				tempStartDate.setHours(10);
				tempStartDate.setMinutes(0);
				startDateStr = convertDate(tempStartDate);
				Date tempEndDate = new Date();
				tempEndDate.setHours(15);
				tempEndDate.setMinutes(0);
				endDateStr = convertDate(tempEndDate);
				*/
				//loop through the list
				jsonWriter.startArray();
				for (Calendar cal : calList){
					jsonWriter.startArrayItem();
					jsonWriter.startObject();
					
					
					
					jsonWriter.startProperty("title");
					jsonWriter.outStringLiteral(cal.getName());
					jsonWriter.endProperty();
					
					jsonWriter.startProperty("start");
					jsonWriter.outDateLiteral(cal.getStartDateTime());
					jsonWriter.endProperty();
					
					jsonWriter.startProperty("end");
					jsonWriter.outDateLiteral(cal.getEndDateTime());
					jsonWriter.endProperty();
					
					jsonWriter.startProperty("allDay");
					jsonWriter.outBooleanLiteral(cal.isAllDay());
					jsonWriter.endProperty();
					
					jsonWriter.startProperty("id");
					jsonWriter.outStringLiteral(cal.getUnid());
					jsonWriter.endProperty();
				
					jsonWriter.startProperty("description");
					jsonWriter.outStringLiteral(cal.getDescription());
					jsonWriter.endProperty();
					
					jsonWriter.startProperty("color");
					jsonWriter.outStringLiteral(cal.getColor());
					jsonWriter.endProperty();
					
					jsonWriter.startProperty("important");
					jsonWriter.outBooleanLiteral(cal.isImportant());
					jsonWriter.endProperty();
					
					//end the item
					jsonWriter.endObject();
					jsonWriter.endArrayItem();
					
					
				}
				
			} else {
				//no documents, send empty array
				jsonWriter.startArray();
				jsonWriter.startArrayItem();
				jsonWriter.endArrayItem();
						
			}
			response.setStatus(HttpServletResponse.SC_OK);
			this.closeJsonWriter();
				
		} catch (Exception e) {
			e.printStackTrace();
			DebugToolbarBean.get().error(e);
		} 
	}


	
	



	

	private void doDelete(HttpServletRequest request, HttpServletResponse response) {
		try {
			this.createErrorJsonWriter();

			jsonWriter.startProperty(JSON_RESPONSE_MESSAGE);
			jsonWriter
					.outStringLiteral("The methode 'DELETE' is not implemented");
			jsonWriter.endProperty();

			this.closeErrorJsonWriter();

			// This should work, but appears to be ignored
			response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}


	private void doPut(HttpServletRequest request, HttpServletResponse response) {
		

		try {
			this.createErrorJsonWriter();
			
			jsonWriter.startProperty(JSON_RESPONSE_MESSAGE);
			jsonWriter
					.outStringLiteral("The methode 'POST' is not implemented");
			jsonWriter.endProperty();

			this.closeErrorJsonWriter();

			// This should work, but appears to be ignored
			response.setStatus(HttpServletResponse.SC_NOT_IMPLEMENTED);

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	
	}


	private void doPost(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			Database calDb;
			calDb = session.getCurrentDatabase();
			Calendar calObj = new Calendar();
			
			String id = "";
			String newStartDate = "";
			String newEndDate = "";
			//test for posting
			boolean isValid = false;
			BufferedReader  reader = request.getReader();
			StringBuffer sb = new StringBuffer();
			try {
				String line;
				while ((line = reader.readLine()) != null) {
					sb.append(line).append('\n');
				}
			} finally {
				reader.close();
			}
			debugMsg("post data " + sb.toString());
			@SuppressWarnings("unchecked")
			final Map<String, Object> jsonObj = (Map<String, Object>) JsonParser.fromJson(JsonJavaFactory.instance,sb.toString());
			
			if (jsonObj.containsKey("id") && jsonObj.containsKey("newStartDate") && jsonObj.containsKey("newEndDate")){
				//get parameters
				id = (String) jsonObj.get("id");
				newStartDate = (String) jsonObj.get("newStartDate");
				newEndDate = (String) jsonObj.get("newEndDate");
				debugMsg("values are " + id + "  " + newStartDate + " " + newEndDate);
				isValid = calObj.changeCalendarDates(calDb, id, newStartDate, newEndDate);
			}
			jsonWriter.startObject();
			jsonWriter.startProperty("status");
			jsonWriter.outBooleanLiteral(isValid);
			jsonWriter.endProperty();
			jsonWriter.endObject();
			response.setStatus(HttpServletResponse.SC_OK);
			jsonWriter.close();
		} catch (Exception e) {
			debugMsg("exception in doPost");
			DebugToolbarBean.get().error(e);
			throw new RuntimeException(e);
		}

	}
	
	private void createErrorJsonWriter() throws ServiceException {
		try {
			jsonWriter.startObject();

			jsonWriter.startProperty(JSON_RESPONSE_STATUS);
			jsonWriter.outStringLiteral(STATUS_ERROR);
			jsonWriter.endProperty();
			

		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	

	
	
	
	
	
	private void closeErrorJsonWriter() throws ServiceException {
		try {
			jsonWriter.endObject();

			jsonWriter.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	private void closeJsonWriter() throws ServiceException {
		try {
			jsonWriter.endArray();
			

			jsonWriter.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings({ "rawtypes" })
	public static void debugMsg(String msg) {

		Map viewscope = (Map) ExtLibUtil.resolveVariable(FacesContext
				.getCurrentInstance(), "viewScope");
		if(viewscope == null) {
			return;
		}
		if (viewscope.containsKey("debug")) {
			if (((String) viewscope.get("debug")).equalsIgnoreCase("true")) {
				DebugToolbarBean.get().info(msg);
			}
		} else if (showDBar) {
			DebugToolbarBean.get().info(msg);
		}
	}
	
	



	
	
}
