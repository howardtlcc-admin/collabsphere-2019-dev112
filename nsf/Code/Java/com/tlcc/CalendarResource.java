package com.tlcc;

import java.util.List;

import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.openntf.domino.Database;

import com.ibm.xsp.extlib.util.ExtLibUtil;

@Path("/calendar")
public class CalendarResource {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Calendar> get(@QueryParam("start") String startParam, @QueryParam("end") String endParam, @QueryParam("important") String importantParam) {
		Database calDb = (Database)ExtLibUtil.resolveVariable("database");
		return Calendar.getCalendarDocs(calDb, startParam, endParam, importantParam);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public boolean update(JsonObject jsonObj) {
		if (jsonObj.containsKey("id") && jsonObj.containsKey("newStartDate") && jsonObj.containsKey("newEndDate")) {
			Calendar calObj = new Calendar();
			Database calDb = (Database)ExtLibUtil.resolveVariable("database");
			
			String id = jsonObj.getString("id");
			String newStartDate = jsonObj.getString("newStartDate");
			String newEndDate = jsonObj.getString("newEndDate");
			return calObj.changeCalendarDates(calDb, id, newStartDate, newEndDate);
		} else {
			return false;
		}
	}
}
